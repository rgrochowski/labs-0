﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class LabDescriptor
    {
        public static Type A = typeof(Tank);
        public static Type B = typeof(Light);
        public static Type C = typeof(Heavy);

        public static string commonMethodName = "TankStatistics";
    }
}
