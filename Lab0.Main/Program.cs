﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class Tank // Klasa bazowa
    {
        // Pola
        protected int armor;
        protected int penetration;
        protected int weight;
        protected string fraction;
        protected string tankType;

        // Właściwości - specjalne metody, ukrycie danych
        public int Armor
        {
            get
            {
                return armor;
            }
            set
            {
                armor = value;
            }
        }

        public int Penetracja
        {
            get
            {
                return penetration;
            }
            set
            {
                penetration = value;
            }
        }

        public int MaxSpeed
        {
            get
            {
                return weight;
            }
            set
            {
                weight = value;
            }
        }

        public string Fraction
        {
            get
            {
                return fraction;
            }
            set
            {
                fraction = value;
            }
        }

        public Tank() // Konstruktor domyślny
        { 

        }
        
        public Tank(string tankType, int armor, int penetration, int weight, string fraction) // Konstruktor własny
        {
            this.armor = armor;
            this.penetration = penetration;
            this.weight = weight;
            this.fraction = fraction;
            this.tankType = tankType;
        }
        
        public virtual string TankStatistics() // Deklaracja głównej metody wirtualnej klasy bazowej - metoda polimorficzna (nie można użyć static, abstract oraz override)
        {
            string statistics = "";
            statistics = "TANK TYPE: " + this.tankType +"\n"+ "ARMOR: " + this.armor +"\n"+ "PENETRATION: " + this.penetration +"\n"+  "WEIGHT: " + this.weight +"\n"+ "FRACTION:" + this.fraction;
            return statistics;
        }
    }


    class Light : Tank // Klasa pchodna pierwsza
    {
        protected int maxAmmo;

        public Light() : base()
        { 

        }

        // Konstruktor klasy pochodnej, wraz z wywołaniem konstruktora klazy bazowej
        public Light(string tankType, int armor, int penetration, int weight, string fraction, int ammo) : base (tankType, armor, penetration, weight, fraction)
        {
           
            this.maxAmmo = ammo;

       }

        public override string TankStatistics() // Przesłanianie głównej metody wirtualnej klasy bazowej - słowo kluczowe override (tylko w klasach pochodnych)
        {
            return base.TankStatistics() +"\n\n"+ "MAX NUMBER OF SHELLS FOR LIGHT TANK: " + this.maxAmmo +"\n\n\n"; // base - tak wywołana metoda traktowana jest jak niewirtualna
        }       
    }

        
    class Heavy : Tank // Klasa pochodna druga
    {
        protected int numberOfCrew;
        
        public Heavy() : base() 
        { 
        }
 
        public Heavy(string tankType, int armor, int penetration, int weight, string fraction, int numberOfCrew) : base(tankType, armor, penetration, weight, fraction)
        {
            this.numberOfCrew = numberOfCrew;
        }
        
      
        public override string TankStatistics()
        {
            return base.TankStatistics() +"\n\n"+ "MAX NUMBER OF CREW FOR HEAVY TANK: " + this.numberOfCrew;
        }
    }

class Program
    {
        static void Main(string[] args)
        {
            // Deklaracja obiektu typu Light, wraz z wartościami zmiennych w tym obiekcie
            Light lightTank = new Light("Light tank", 40, 100, 10000, " Germany", 75);
            Console.WriteLine(lightTank.TankStatistics());

            // Deklaracja obiektu typu Heavy, wraz z wartościami zmiennych w tym obiekcie
            Heavy heavyTank = new Heavy("Heavy tank", 150, 300, 50000, " Germany", 6);
            Console.WriteLine(heavyTank.TankStatistics());

            Console.ReadKey();
        }
    }
}
